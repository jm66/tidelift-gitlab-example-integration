FROM alpine:latest
RUN apk add --update --no-cache curl jq bash && \
    rm -rf /var/cache/apk/*
ADD tidelift-scan.sh /
RUN chmod +x tidelift-scan.sh
