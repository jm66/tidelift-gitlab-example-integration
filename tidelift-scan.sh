#!/usr/bin/env bash

# Tidelift integration script
# Author: jwmarcus
# License: MIT

# For more information about setting these values, see the
# documentation at https://tidelift.com/docs/subscriber/api
TEAM="${TL_TEAM:-please-set-team}"
PROJECT="${TL_PROJECT:-please-set-project}"
BRANCH="${CI_COMMIT_REF_NAME:-master}"
TOKEN="${TL_TOKEN:-please-set-api-token}"

HEADER="Authorization: Bearer repository/$TOKEN"
BASE_URI="https://api.tidelift.com/subscriber/$TEAM/$PROJECT"
SUBMIT_URI="$BASE_URI/manifest/upload?branch=$BRANCH"
MANIFESTS=( $@ )

# Submit the relavent file(s) for this project.
echo "[INFO] Submitting manifest files to Tidelift"

SUBMIT_CODE=""
case ${#MANIFESTS[@]} in
  0)
    echo "[CRIT] No manifest passed to script."
    exit 1
    ;;
  1)
    SUBMIT_CODE=$(curl -s -o submit_response.temp \
      -w "%{http_code}" -X POST \
      -F "files[]=@$1" \
      -H "$HEADER" "$SUBMIT_URI")
    ;;
  2)
    SUBMIT_CODE=$(curl -s -o submit_response.temp \
      -w "%{http_code}" -X POST \
      -F "files[]=@$1" \
      -F "files[]=@$2" \
      -H "$HEADER" "$SUBMIT_URI")
    ;;
esac

if [ $SUBMIT_CODE != "201" ]; then
  echo "[CRIT] Submission to Tidelift failed. Code: $SUBMIT_CODE"
  cat submit_response.temp
  exit 1
fi

# Parse out the revision number from the submission response
REVISION=$(jq -r ".revision" < submit_response.temp)
STATUS_URI="$BASE_URI/$REVISION/status"
echo "[INFO] Revision received, revision=$REVISION"

# Immediately after submission, we will get a 404. Therefore, wait until
# a new scan has begun before polling the endpoint for valid JSON.
while true; do
  STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}" -H "$HEADER" "$STATUS_URI")

  if [ "$STATUS_CODE" = 200 ]; then
    while true; do
      STATUS_RES=$(curl -s -H "$HEADER" "$STATUS_URI")
      SCAN_STATUS=$(echo "$STATUS_RES" | jq -r ".status")

      if [ "$SCAN_STATUS" = "failure" ]; then
        echo $STATUS_RES
        exit 1
      elif [ "$SCAN_STATUS" = "error" ]; then
        echo $STATUS_RES
        exit 1
      elif [ "$SCAN_STATUS" = "success" ]; then
        echo $STATUS_RES
        exit 0
      fi

      echo "[INFO] Waiting for scan to complete. Status:$SCAN_STATUS"; sleep 5
    done
  fi
  echo "[INFO] Waiting for confirmed submission. Code:$STATUS_CODE"; sleep 3

done
